# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ding package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-18 22:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: askConfirmPopup.js:36 askNamePopup.js:37 desktopIconsUtil.js:211
#: desktopManager.js:1481
msgid "Cancel"
msgstr ""

#: askConfirmPopup.js:37
msgid "Delete"
msgstr ""

#: askNamePopup.js:36
msgid "OK"
msgstr ""

#: askRenamePopup.js:40
msgid "Folder name"
msgstr ""

#: askRenamePopup.js:40
msgid "File name"
msgstr ""

#: askRenamePopup.js:47
msgid "Rename"
msgstr ""

#: desktopIconsUtil.js:80
msgid "Command not found"
msgstr ""

#: desktopIconsUtil.js:202
msgid "Do you want to run “{0}”, or display its contents?"
msgstr ""

#: desktopIconsUtil.js:203
msgid "“{0}” is an executable text file."
msgstr ""

#: desktopIconsUtil.js:207
msgid "Execute in a terminal"
msgstr ""

#: desktopIconsUtil.js:209
msgid "Show"
msgstr ""

#: desktopIconsUtil.js:213
msgid "Execute"
msgstr ""

#: desktopManager.js:135
msgid "Nautilus File Manager not found"
msgstr ""

#: desktopManager.js:136
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""

#: desktopManager.js:563
msgid "New Folder"
msgstr ""

#: desktopManager.js:567
msgid "New Document"
msgstr ""

#: desktopManager.js:572
msgid "Paste"
msgstr ""

#: desktopManager.js:576
msgid "Undo"
msgstr ""

#: desktopManager.js:580
msgid "Redo"
msgstr ""

#: desktopManager.js:586
msgid "Select all"
msgstr ""

#: desktopManager.js:592
msgid "Show Desktop in Files"
msgstr ""

#: desktopManager.js:596 fileItem.js:913
msgid "Open in Terminal"
msgstr ""

#: desktopManager.js:602
msgid "Change Background…"
msgstr ""

#: desktopManager.js:611
msgid "Display Settings"
msgstr ""

#: desktopManager.js:618
msgid "Desktop Icons settings"
msgstr ""

#: desktopManager.js:629
msgid "Scripts"
msgstr ""

#: desktopManager.js:1148 desktopManager.js:1196
msgid "Error while deleting files"
msgstr ""

#: desktopManager.js:1222
msgid "Are you sure you want to permanently delete these items?"
msgstr ""

#: desktopManager.js:1223
msgid "If you delete an item, it will be permanently lost."
msgstr ""

#: desktopManager.js:1339
msgid "New folder"
msgstr ""

#: desktopManager.js:1414
msgid "Can not email a Directory"
msgstr ""

#: desktopManager.js:1415
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""

#: desktopManager.js:1479
msgid "Select Extract Destination"
msgstr ""

#: desktopManager.js:1482
msgid "Select"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:153
msgid "Home"
msgstr ""

#: fileItem.js:809
msgid "Open All..."
msgstr ""

#: fileItem.js:809
msgid "Open"
msgstr ""

#: fileItem.js:816
msgid "Open All With Other Application..."
msgstr ""

#: fileItem.js:816
msgid "Open With Other Application"
msgstr ""

#: fileItem.js:820
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: fileItem.js:828
msgid "Cut"
msgstr ""

#: fileItem.js:831
msgid "Copy"
msgstr ""

#: fileItem.js:835
msgid "Rename…"
msgstr ""

#: fileItem.js:839
msgid "Move to Trash"
msgstr ""

#: fileItem.js:843
msgid "Delete permanently"
msgstr ""

#: fileItem.js:849
msgid "Don't Allow Launching"
msgstr ""

#: fileItem.js:849
msgid "Allow Launching"
msgstr ""

#: fileItem.js:856
msgid "Empty Trash"
msgstr ""

#: fileItem.js:863
msgid "Eject"
msgstr ""

#: fileItem.js:870
msgid "Unmount"
msgstr ""

#: fileItem.js:885
msgid "Extract Here"
msgstr ""

#: fileItem.js:888
msgid "Extract To..."
msgstr ""

#: fileItem.js:893
msgid "Send to..."
msgstr ""

#: fileItem.js:897
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] ""
msgstr[1] ""

#: fileItem.js:900
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] ""
msgstr[1] ""

#: fileItem.js:905
msgid "Common Properties"
msgstr ""

#: fileItem.js:905
msgid "Properties"
msgstr ""

#: fileItem.js:909
msgid "Show All in Files"
msgstr ""

#: fileItem.js:909
msgid "Show in Files"
msgstr ""

#: preferences.js:91
msgid "Settings"
msgstr ""

#: preferences.js:98
msgid "Size for the desktop icons"
msgstr ""

#: preferences.js:98
msgid "Tiny"
msgstr ""

#: preferences.js:98
msgid "Small"
msgstr ""

#: preferences.js:98
msgid "Standard"
msgstr ""

#: preferences.js:98
msgid "Large"
msgstr ""

#: preferences.js:99
msgid "Show the personal folder in the desktop"
msgstr ""

#: preferences.js:100
msgid "Show the trash icon in the desktop"
msgstr ""

#: preferences.js:101 schemas/org.gnome.shell.extensions.ding.gschema.xml:38
msgid "Show external drives in the desktop"
msgstr ""

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:43
msgid "Show network drives in the desktop"
msgstr ""

#: preferences.js:105
msgid "New icons alignment"
msgstr ""

#: preferences.js:106
msgid "Top-left corner"
msgstr ""

#: preferences.js:107
msgid "Top-right corner"
msgstr ""

#: preferences.js:108
msgid "Bottom-left corner"
msgstr ""

#: preferences.js:109
msgid "Bottom-right corner"
msgstr ""

#: preferences.js:111 schemas/org.gnome.shell.extensions.ding.gschema.xml:48
msgid "Add new drives to the opposite side of the screen"
msgstr ""

#: preferences.js:112
msgid "Highlight the drop place during Drag'n'Drop"
msgstr ""

#: preferences.js:116
msgid "Settings shared with Nautilus"
msgstr ""

#: preferences.js:122
msgid "Click type for open files"
msgstr ""

#: preferences.js:122
msgid "Single click"
msgstr ""

#: preferences.js:122
msgid "Double click"
msgstr ""

#: preferences.js:123
msgid "Show hidden files"
msgstr ""

#: preferences.js:124
msgid "Show a context menu item to delete permanently"
msgstr ""

#: preferences.js:129
msgid "Action to do when launching a program from the desktop"
msgstr ""

#: preferences.js:130
msgid "Display the content of the file"
msgstr ""

#: preferences.js:131
msgid "Launch the file"
msgstr ""

#: preferences.js:132
msgid "Ask what to do"
msgstr ""

#: preferences.js:138
msgid "Show image thumbnails"
msgstr ""

#: preferences.js:139
msgid "Never"
msgstr ""

#: preferences.js:140
msgid "Local files only"
msgstr ""

#: preferences.js:141
msgid "Always"
msgstr ""

#: prefs.js:37
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Desktop Icons settings'"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:18
msgid "Icon size"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:19
msgid "Set the size for the desktop icons."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:23
msgid "Show personal folder"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:24
msgid "Show the personal folder in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:28
msgid "Show trash icon"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:29
msgid "Show the trash icon in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:33
msgid "New icons start corner"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:34
msgid "Set the corner from where the icons will start to be placed."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:39
msgid "Show the disk drives connected to the computer."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:44
msgid "Show mounted network volumes in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:49
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:53
msgid "Shows a rectangle in the destination place during DnD"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:54
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
